const addMovieButton = document.getElementById('add');
const addMovieModal = document.getElementById('add-modal');
const backdrop = document.getElementById('backdrop');
const cancelButton = document.getElementById('cancel-button');
const addButton = document.getElementById('add-button');
const userInputs = document.querySelectorAll('input');
const entryTextSection = document.getElementById('entry-text');
const listRoot = document.getElementById('movie-list');
const closeDeleteMovieModal = document.getElementById('delete-modal');
const movies = [];

const updateUI = () => {
  if (movies.length === 0) {
    entryTextSection.style.display = 'block';
  } else {
    entryTextSection.style.display = 'none';
  }
};

const toggleBackdrop = () => {
  backdrop.classList.toggle('visible');
};

const closeMovieModal = () => {
  addMovieModal.classList.remove('visible');
};

const showMovieModalHandler = () => {
  addMovieModal.classList.add('visible');
  toggleBackdrop();
};

const cancelMovieDeletion = () => {
  toggleBackdrop();
  closeDeleteMovieModal.classList.remove('visible');
};

const deleteMovie = (movieId) => {
  let movieIndex = 0;
  for (const movie of movies) {
    if (movie.id === movieId) {
      break;
    }
    movieIndex++;
  }
  movies.splice(movieIndex, 1);
  listRoot.children[movieIndex].remove();
  cancelMovieDeletion();
  updateUI();
};

const deleteMovieModalHandler = (movieId) => {
  closeDeleteMovieModal.classList.add('visible');
  toggleBackdrop();
  const cancelDeletionButton = closeDeleteMovieModal.querySelector('.btn--passive');
  let confirmDeletionButton = closeDeleteMovieModal.querySelector('.btn--danger');
  confirmDeletionButton.replaceWith(confirmDeletionButton.cloneNode(true));
  confirmDeletionButton = closeDeleteMovieModal.querySelector('.btn--danger');
  cancelDeletionButton.removeEventListener('click', cancelMovieDeletion);
  cancelDeletionButton.addEventListener('click', cancelMovieDeletion);
  confirmDeletionButton.addEventListener('click', deleteMovie.bind(null, movieId));
};

const renderNewMovieElement = (id, title, image, rating) => {
  const newMovieElement = document.createElement('li');
  newMovieElement.className = 'movie-element';
  newMovieElement.innerHTML = `
    <div class="movie-element__image">
    <img src="${image}" alt="${title}">
    </div>
    <div class="movie-element__info">
    <h2>${title}</h2>
    <p>${rating}/5 stars</p>
</div>
  `;
  newMovieElement.addEventListener('click', deleteMovieModalHandler.bind(null, id));
  listRoot.append(newMovieElement);
};

const clearInputs = () => {
  for (const usrInput of userInputs) {
    usrInput.value = '';
  }
};

const backdropClickHandler = () => {
  closeMovieModal();
  cancelMovieDeletion();
  clearInputs();
};

const cancelAddingMovieHandler = () => {
  closeMovieModal();
  clearInputs();
  toggleBackdrop();
};

const addMovieHandler = () => {
  const titleValue = userInputs[0].value;
  const imageUrlValue = userInputs[1].value;
  const ratingValue = userInputs[2].value;

  if (titleValue.trim() === ''
      || imageUrlValue.trim() === ''
      || ratingValue.trim() === ''
      || +ratingValue < 1
      || +ratingValue > 5) {
    alert('Please enter values (Rating between 1 and 5 and all fields should be filled)');
  } else {
    const newMovie = {
      id: Math.random().toString(),
      title: titleValue,
      image: imageUrlValue,
      rating: ratingValue,
    };

    movies.push(newMovie);
    console.log(movies);
    closeMovieModal();
    toggleBackdrop();
    clearInputs();
    renderNewMovieElement(newMovie.id, newMovie.title, newMovie.image, newMovie.rating);
    updateUI();
  }
};

addMovieButton.addEventListener('click', showMovieModalHandler);
backdrop.addEventListener('click', backdropClickHandler);
cancelButton.addEventListener('click', cancelAddingMovieHandler);
addButton.addEventListener('click', addMovieHandler);
